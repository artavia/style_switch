# Brief description
This is the result that I came up with after visiting a post originally published at SitePoint on the subject of creating a simple style switcher.


## Please visit
You can see [the project](https://artavia.gitlab.io/style_switch "the project at github") for yourself and decide whether it is really for you or not. 


## I cannot take sole credit
This item would not have been possible if it were not for the impetus inspired after visiting the [original post](https://www.sitepoint.com/creating-simple-style-switcher/ "Link to original post at SitePoint") at SitePoint. I felt it&rsquo;s obligatory that I modernize the use case. Anybody can lift&hellip; that&rsquo;s easy! What I set out to do is leave a practical take&#45;away for the next person.


## The Web Storage object
What you do is select your favorite default style. Then, you can close your browser, visit the same site, and your style preferences from your previous web session will load thanks to web storage. For this post, I use the Web Storage object which lines up with progressive enhancement. While on the subject, why not create an alternative experience, I thought. Thus, cookies for graceful degradation were factored in, too. 


### A future exercise to consider
Refactoring this exercise originally showed me a path that would facilitate the creation of similar logic for a default&#45;language chooser. That case, however, used anchor tags and not the select tag with its accompanying baggage.