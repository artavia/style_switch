var donLuchoHAKI = ( function ( ) {
	
	/*################  ES5 pragma  ######################*/
	'use strict';	
	
	function _AddEventoHandler( nodeFlanders, type, callback ) {
		if( type !== "DOMContentLoaded") { 
			if( nodeFlanders.addEventListener ) { 
				nodeFlanders.addEventListener( type, callback, false);
			}		
			else	
			if( nodeFlanders.attachEvent ) { 
				nodeFlanders.attachEvent( "on" + type, callback );
			} 		
			else { 
				nodeFlanders["on" + type] = callback; 
			}
		}
		else 
		if( type === "DOMContentLoaded" ) { 
			if( nodeFlanders.addEventListener ) { 
				nodeFlanders.addEventListener( type, callback, false);
			}
			else 
			if( nodeFlanders.attachEvent ) { 
				if( nodeFlanders.readyState === "loading" ) {
					nodeFlanders.onreadystatechange = callback;
				}
			}
			else { 
				nodeFlanders["on" + type] = callback; 
			}
		}
	}

	function _RetEVTsrcEL_evtTarget( leEvt ) { 
		if( typeof leEvt !== "undefined") { 
			var _EREF = leEvt;
		}
		else {
			var _EREF = window.event; 
		}
		if( typeof _EREF.target !== "undefined") {
			var evtTrgt = _EREF.target;	
		}
		else {
			var evtTrgt = _EREF.srcElement;
		}
		return evtTrgt;
	}

	// END of _private properties 
	return { 
		Utils : {
			evt_u : {	
				AddEventoHandler : function( nodeFlanders, type, callback ) {
					return _AddEventoHandler( nodeFlanders, type, callback );
				} , 
				RetEVTsrcEL_evtTarget : function( leEvt ) {
					return _RetEVTsrcEL_evtTarget( leEvt );
				}
			}
		} 
	}; // END public properties
	
}( ) ); // console.log( donLuchoHAKI ); 

/* ###################################################### */

var Gh_pages_styleswitch = ( function() {	
	/*################  ES5 pragma  ######################*/
	'use strict';

	var _docObj = window.document;
	var _bodyObj = _docObj.body;
	var _el_console = _docObj.getElementById("console");
	var _el_switcheroo;
	var _allSwitchOptions;
	var _h;
	var _SwtchRooIDX;
	var _SwtchRooVLU;
	
	var _COOKIE_DEATH = -1;
	var _COOKIE_LIFE = 365;	
	var _BODYCLASSNAME = "bodyClassName";
	
	var _obj_locsto;

	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@  COOKIES ++ _ToastCookie  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	function _ToastCookie( pmNamecookie ) { 
		_SetCookie( pmNamecookie, "", _COOKIE_DEATH );
	} 
	
	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  COOKIES ++ _GetCookie  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	function _GetCookie( pmNamecookie ) {
		var c_stringWhole = pmNamecookie + "=";
		var ar_cookies = _docObj.cookie.split(';');
		var i; 
		for( i = 0; i < ar_cookies.length; i = i + 1 ) {
			var c_name = ar_cookies[ i ];
			while (c_name.charAt( 0 ) === ' ' ) {
				c_name = c_name.substring( 1, c_name.length );
			}
			if ( c_name.indexOf( c_stringWhole ) === 0 ) {
				return c_name.substring( c_stringWhole.length, c_name.length );
			}
		}
		return null;
	} 
	
	/*   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@  COOKIES ++ _SetCookie  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  */
	function _SetCookie( pmNamecookie, pmValcookie, pmDayscookie ) {
		if ( pmDayscookie ) {
			var expDate = new Date();
			expDate.setTime( expDate.getTime() + ( pmDayscookie * 24*60*60*1000 ) );
			var str_Expires = "; expires=" + expDate.toGMTString();
			_docObj.cookie = pmNamecookie + "=" + pmValcookie + str_Expires + "; path=/";
			expDate = null;
		}
		else {
			var str_Expires = "";
			_docObj.cookie = pmNamecookie + "=" + pmValcookie + str_Expires +"; path=/";
		}
	} 
	
	function _DefaultStyleSwitcher( leEvt ) { 
		
		if( !!( "localStorage" in window ) ) { 
			var evtTrgt = donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt );
			_el_switcheroo = _docObj.getElementById( evtTrgt.id );	
			_allSwitchOptions = _el_switcheroo.getElementsByTagName("option");
			
			_SwtchRooIDX = _allSwitchOptions[ _el_switcheroo.selectedIndex ];
			_SwtchRooVLU = _SwtchRooIDX.value;	
			
			if( !!(_SwtchRooVLU) ) { 
				if( _SwtchRooVLU !== "" ) { 
					_obj_locsto.setItem( "bodyClassName" , _SwtchRooVLU ); 
					_bodyObj.className = _obj_locsto.getItem("bodyClassName");
					_el_console.innerHTML = _obj_locsto.getItem("bodyClassName"); 
				}
			}

			else
			if( !(_SwtchRooVLU) ) { 
				if( _SwtchRooVLU === "" ) {
					_el_console.innerHTML = "I didn\'t exactly have this in mind. Tighten it up!"; 
				}
			}
		} // END if( !!( "localStorage" in window ) )
		else 
		if( !( "localStorage" in window ) ) {
			var evtTrgt = donLuchoHAKI.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt );
			_el_switcheroo = _docObj.getElementById( evtTrgt.id );	
			_allSwitchOptions = _el_switcheroo.getElementsByTagName("option");
			
			_SwtchRooIDX = _allSwitchOptions[ _el_switcheroo.selectedIndex ];
			_SwtchRooVLU = _SwtchRooIDX.value;	
			
			if( !!(_SwtchRooVLU) ) { 
				if( _SwtchRooVLU !== "" ) { 
					// >>> when assigning a DOM element value to a cookie string value USE >>> escape(str)
					_SetCookie( _BODYCLASSNAME, escape( _SwtchRooVLU ), _COOKIE_LIFE );
					_bodyObj.className = unescape( _GetCookie(_BODYCLASSNAME) );
					_el_console.innerHTML = unescape( _GetCookie(_BODYCLASSNAME) ); 
				}
			}
			else
			if( !(_SwtchRooVLU) ) { 
				if( _SwtchRooVLU === "" ) {
					_el_console.innerHTML = "I didn\'t exactly have this in mind. Tighten it up!"; 
				}
			}
		} // END if( !( "localStorage" in window ) )  
		
	} // END function 

	function _LOAD() {
		// console.log( 'now it\'s loaded' );
		if(window.navigator.cookieEnabled &&(!!(window.localStorage)|| !(window.localStorage))){			
			
			if( !!( "localStorage" in window ) ) { 
				_el_switcheroo = _docObj.getElementById("switcheroo");
				_allSwitchOptions = _el_switcheroo.getElementsByTagName("option");
				
				_obj_locsto = window.localStorage || {}; // LOCAL STORAGE ~ ASSIGNMENT
				if( !!( _obj_locsto.getItem("bodyClassName") ) ) { // LOCAL STORAGE ~ GET
					_bodyObj.className = _obj_locsto.getItem("bodyClassName"); 
					_el_console.innerHTML = _obj_locsto.getItem("bodyClassName");
				} 
				else 
				if( !( _obj_locsto.getItem("bodyClassName") ) ) { // LOCAL STORAGE ~ GET
					_el_console.innerHTML = "You can set up a default style in seconds!";
				} 
				
				for( _h = 0; _h < _allSwitchOptions.length; _h = _h + 1 ){ // LOCAL STORAGE ~ GET
					if( _allSwitchOptions[_h].value === _obj_locsto.getItem("bodyClassName")){
						_el_switcheroo.selectedIndex = _h; 
						_el_console.innerHTML = _allSwitchOptions[_h].value; 
					} 
				} 
				donLuchoHAKI.Utils.evt_u.AddEventoHandler(_el_switcheroo,"change",_DefaultStyleSwitcher );
			} // END if( !!( "localStorage" in window ) )
			else 
			if( !( "localStorage" in window ) ) {
				_el_switcheroo = _docObj.getElementById("switcheroo");	
				_allSwitchOptions = _el_switcheroo.getElementsByTagName("option");
				
				if( !!( _GetCookie(_BODYCLASSNAME) ) ) { // >>> when printing out a cookie value USE >>> unescape(encodedStr) 
					_bodyObj.className = unescape( _GetCookie(_BODYCLASSNAME) ); 
					_el_console.innerHTML = unescape( _GetCookie(_BODYCLASSNAME) );
				}
				else 
				if( !( _GetCookie(_BODYCLASSNAME) ) ) { // >>> when printing out a cookie value USE >>> unescape(encodedStr) 
					_el_console.innerHTML = "You can set up a default style in seconds!";
				} 
				
				for( _h = 0; _h < _allSwitchOptions.length; _h = _h + 1 ){ // >>> when printing out a cookie value USE >>> unescape(encodedStr)
					if ( _allSwitchOptions[_h].value === unescape( _GetCookie(_BODYCLASSNAME) ) ){ 
						_el_switcheroo.selectedIndex = _h; 
						_el_console.innerHTML = _allSwitchOptions[_h].value; 
					} 
				} 
				donLuchoHAKI.Utils.evt_u.AddEventoHandler(_el_switcheroo,"change",_DefaultStyleSwitcher );
			} // END if( !( "localStorage" in window ) ) 
			
		} // END if(window.navigator.cookieEnabled && (!!(window.localStorage) || !(window.localStorage)))
		else 
		if(!window.navigator.cookieEnabled){
			_el_console.innerHTML = "<p>"+"Adjust device settings so it can permit cookies and so you can navigate properly"+"</p>"; 
		} // END if(!window.navigator.cookieEnabled)
		//
	}
	// END of _private properties

	return {		
		InitLoad: function() {
			return _LOAD(); 
		} // window.Gh_pages_styleswitch.InitLoad()
	};
} )(); // window.Gh_pages_styleswitch

donLuchoHAKI.Utils.evt_u.AddEventoHandler( window, "load" , Gh_pages_styleswitch.InitLoad() ); 